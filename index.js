fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(json => console.log(`title : ${json.title}
body : ${json.body}`))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method : 'PATCH',
	headers : {
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		title : 'Corrected post',
		body : 'Hello!'
	}),
})
.then((response) => {return response.json()})
.then((json) => console.log(json))